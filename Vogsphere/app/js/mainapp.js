﻿var mainApp = angular.module('mainapp', ['ngRoute', 'ngMaterial']);

mainApp.config(['$routeProvider',
  function ($routeProvider) {
      $routeProvider.
        when('/start', {
            templateUrl: '/app/html/start.html',
            controller: 'startCtrl'
        }).
        when('/scan', {
            templateUrl: '/app/html/scan.html',
            controller: 'scanCtrl'
        }).
        when('/search', {
            templateUrl: '/app/html/search.html',
            controller: 'searchCtrl'
        }).
        when('/newdocs', {
            templateUrl: '/app/html/newdocs.html',
            controller: 'newdocsCtrl'
        }).
          when('/edit/:docId', {
              templateUrl: '/app/html/editdoc.html',
              controller: 'editCtrl'
          }).
        otherwise({
            redirectTo: '/start'
        });
  }]);

mainApp.controller('editCtrl', ['$scope', '$http', '$routeParams', '$location',
    function ($scope, $http, $routeParams, $location) {
        $scope.docId = $routeParams.docId;
        $scope.document = {};
        $scope.loadingDone = false;
        $scope.tags = [];
        $scope.selectedItem = null;
        $scope.searchText = null;

        $scope.submitEdit = function () {
            var keywords = [];
            for (var i = 0; i < $scope.document.Keywords.length; i++) {
                keywords.push($scope.document.Keywords[i].text);
            }

            $scope.document.Keywords = keywords;

            $http.post('/api/doc/' + $scope.docId + "/save",
                JSON.stringify($scope.document)).then(function (response) {
                    var nextDocId = response.data;
                    $location.path("edit/" + nextDocId);
                });
        };

        $scope.submitDelete = function () {
            $http.get('/api/doc/' + $scope.docId + "/delete").then(function (response) {
                var nextDocId = response.data;
                $location.path("edit/" + nextDocId);
            });
        };

        $http.get('/api/doc/' + $scope.docId).then(function (response) {
            $scope.document = response.data;
            $scope.document.DocumentDate = new Date(response.data.DocumentDate);
            var kw = [];
            for (var i = 0; i < $scope.document.Keywords.length; i++) {
                kw.push({ text: $scope.document.Keywords[i] });
            }
            $scope.document.Keywords = kw;
            $scope.loadingDone = true;
        });

        $http.get('/api/docs/keywords').then(function (response) {
            $scope.tags = [];
            console.log(response.data);
            for (var i = 0; i < response.data.length; i++) {
                $scope.tags.push({ text: response.data[i] });
            }
        });

        $scope.querySearch = function (query) {
            console.log("query");

            var lowerQuery = angular.lowercase(query);
            var results = [];
            for (var i = 0; i < $scope.tags.length; i++) {
                var lowerTag = angular.lowercase($scope.tags[i].text);
                if (lowerTag.indexOf(lowerQuery) !== -1) {
                    results.push($scope.tags[i]);
                }
            }

            return results;
        }
        $scope.createFilterFor = function (query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(vegetable) {
                return (vegetable._lowername.indexOf(lowercaseQuery) === 0) ||
                    (vegetable._lowertype.indexOf(lowercaseQuery) === 0);
            };

        }
        $scope.transformChip = function (chip) {
            console.log("transform");
            // If it is an object, it's already a known chip
            if (angular.isObject(chip)) {
                return chip;
            }

            // Otherwise, create a new one
            return { text: chip }
        }

    }]);

mainApp.controller('startCtrl', ['$scope', '$http',
    function ($scope, $http) {
        $scope.ctTotal = 120;
        $scope.ctNew = 20;
        $scope.loadingDone = false;
    }]);

mainApp.controller('scanCtrl', ['$scope', '$http', 'backendHubProxy',
    function ($scope, $http, backendHubProxy) {
        $scope.curFile = "Init";
        $scope.scannedCt = 1;
        $scope.totalCt = 1;

        $scope.getPercentage = function () {
            return ($scope.scannedCt / $scope.totalCt) * 100.0;
        };

        var scanHub = backendHubProxy('scanHub');
        scanHub.on("Progress", function (data) {
            $scope.curFile = data.file;
            $scope.scannedCt = data.scannedCt;
            $scope.totalCt = data.totalCt;
            $scope.scanning = true;
        });
        $scope.scanning = false;
        $scope.startScan = function () {
            $scope.scanning = true;

            $http.get('/api/docs/scan').then(function (response) {
                $scope.scanning = false;

            });
        }
    }]);

mainApp.controller('newdocsCtrl', ['$scope', '$http', '$location',
    function ($scope, $http, $location) {
        $scope.newDocs = [];

        $scope.edit = function (d) {
            $location.path("edit/" + d.DocumentId);
        };

        $http.get('/api/docs/new').then(function (response) {
            $scope.newDocs = response.data;
        });
    }]);

mainApp.controller('searchCtrl', ['$scope', '$http', '$location',
    function ($scope, $http, $location) {
        $scope.docs = [];

        $scope.edit = function (d) {
            $location.path("edit/" + d.DocumentId);
        };
        $scope.view = function (d) {
            window.open("/api/doc/" + d.DocumentId + "/pdf");
        };
        $http.get('/api/docs/search/all/0').then(function (response) {
            $scope.docs = response.data;
        });

        $scope.getDisplayDate = function (d) {
            var dateobj = new Date(d.DocumentDate);

            function pad(n) { return n < 10 ? "0" + n : n; }
            var result = pad(dateobj.getDate()) + "." + pad(dateobj.getMonth() + 1) + "." + dateobj.getFullYear();
            return result;
        }

    }]);

mainApp.factory('backendHubProxy', ['$rootScope',
  function ($rootScope) {

      function backendFactory(hubName) {
          var connection = $.hubConnection();
          var proxy = connection.createHubProxy(hubName);
          connection.logging = true;

          //hook up the ping
          proxy.on("ping", function () { console.log("Got pinged"); });

          connection.start().done(function () {
              console.log("CONNECTION ESTABLISHED");
          });

          return {
              on: function (eventName, callback) {
                  proxy.on(eventName, function (result) {
                      $rootScope.$apply(function () {
                          if (callback) {
                              callback(result);
                          }
                      });
                  });
              },
              invoke: function (methodName, callback) {
                  proxy.invoke(methodName)
                  .done(function (result) {
                      $rootScope.$apply(function () {
                          if (callback) {
                              callback(result);
                          }
                      });
                  });
              }
          };
      };

      return backendFactory;
  }]);