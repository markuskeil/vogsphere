﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Hosting;
using System.Web.Http;
using Microsoft.AspNet.SignalR;
using Vogsphere.Models;

namespace Vogsphere.Controllers
{
    [RoutePrefix("api/doc/{docid:guid}")]
    public class DocumentApiController : ApiController
    {
        [HttpGet]
        [Route("")]
        public PdfDocument GetDoc(Guid docid)
        {
            PdfDocumentRepository pdr = new PdfDocumentRepository();
            var doc = pdr.GetDocument(docid);
            return doc;
        }

        [HttpPost]
        [Route("save")]
        public Guid SaveDoc([FromBody] PdfDocument doc)
        {
            PdfDocumentRepository pdr = new PdfDocumentRepository();
            pdr.SaveDocument(doc);
            return pdr.NextNewDocumentId();
        }

        [HttpGet]
        [Route("delete")]
        public Guid DeleteDoc(Guid docid)
        {
            PdfDocumentRepository pdr = new PdfDocumentRepository();
            pdr.DeleteDocument(docid);
            return pdr.NextNewDocumentId();
        }

        [HttpGet]
        [Route("thumb")]
        public HttpResponseMessage GetThumb(Guid docid)
        {
            PdfDocumentRepository pdr = new PdfDocumentRepository();
            var thumb = pdr.GetThumbnail(docid);

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(thumb)
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");
            return result;
        }

        [HttpGet]
        [Route("smallthumb")]
        public HttpResponseMessage GetSmallThumb(Guid docid)
        {
            PdfDocumentRepository pdr = new PdfDocumentRepository();
            var thumb = pdr.GetThumbnail(docid);

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(ResizeImage(thumb, 0.2))
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");
            return result;
        }

        [HttpGet]
        [Route("pdf")]
        public HttpResponseMessage GetPdf(Guid docid)
        {
            PdfDocumentRepository pdr = new PdfDocumentRepository();
            var thumb = pdr.GetPdf(docid);

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(thumb)
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            return result;
        }

        public static byte[] ResizeImage(byte[] imageBytes, double scale)
        {
            Image image = new Bitmap(new MemoryStream(imageBytes));
            double width = image.Width*scale;
            double height = image.Height*scale;
            var destRect = new Rectangle(0, 0, (int) width, (int) height);
            var destImage = new Bitmap((int)width, (int)height);

            destImage.SetResolution(96, 96);

            using (var graphics = Graphics.FromImage(destImage))
            {
                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            MemoryStream ms = new MemoryStream();
            destImage.Save(ms,ImageFormat.Jpeg);
            return ms.ToArray();
        }

    }

    [RoutePrefix("api/docs")]
    public class DocumentManageApiController : ApiController
    {
        [HttpGet]
        [Route("new")]
        public List<PdfDocument> ListNew()
        {
            PdfDocumentRepository pdr = new PdfDocumentRepository();
            return pdr.ListNew();
        }

        [HttpGet]
        [Route("search/{mode}/{query}")]
        public List<PdfDocument> Search(string mode,string query)
        {
            PdfDocumentRepository pdr = new PdfDocumentRepository();
            return pdr.Search(mode,query);
        }

        [HttpGet]
        [Route("keywords")]
        public List<string> KnownKeywords()
        {
            PdfDocumentRepository pdr = new PdfDocumentRepository();
            return pdr.DistinctKeywords().ToList();
        }

        [HttpGet]
        [Route("scan")]
        public bool ScanInput()
        {
            List<string> newfiles = Directory.GetFiles(ConfigurationManager.AppSettings["scandir"]).ToList();
            IHubContext hc = GlobalHost.ConnectionManager.GetHubContext<ScanHub>();
            int curCount = 0;
            foreach (string pdffile in newfiles)
            {
                var fi = new FileInfo(pdffile);
                if (fi.Extension != ".pdf") continue;

                hc.Clients.All.Progress(new
                {
                    file = fi.Name,
                    totalCt = newfiles.Count,
                    scannedCt = curCount
                });
                PdfDocumentRepository pdr = new PdfDocumentRepository
                {
                    OcrDataPath = HostingEnvironment.MapPath("~/libs/tesseract-ocr/tessdata"),
                    TempPath = HostingEnvironment.MapPath("~/temp")
                };

                pdr.CreateFromFile(pdffile);
                
                File.Move(pdffile,Path.Combine(ConfigurationManager.AppSettings["importeddir"],fi.Name));
                curCount++;
            }

            return true;
        }

        [Route("statistics")]
        public object Statistics()
        {
            PdfDocumentRepository pdr = new PdfDocumentRepository
            {
                OcrDataPath = HostingEnvironment.MapPath("~/libs/tesseract-ocr/tessdata"),
                TempPath = HostingEnvironment.MapPath("~/temp")
            };

            return new
            {
                ctTotal = pdr.DocumentCountTotal,
                ctNew = pdr.DocumentCountNew
            };

        }
    }
}



