﻿using System.Web.Mvc;

namespace Vogsphere.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return Redirect("~/app/index.html");
        }
    }
}