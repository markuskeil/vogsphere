using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Text;
using Ghostscript.NET;
using Ghostscript.NET.Rasterizer;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;


namespace Vogsphere.Models
{
    public class PdfDocumentRepository
    {

        private readonly GhostscriptVersionInfo _lastInstalledVersion;
        private readonly IMongoDatabase _mcFileDb;
        private readonly IMongoDatabase _mcMainDb;

        public string OcrDataPath { get; set; }
        public string TempPath { get; set; }
        public int DocumentCountTotal { get; set; }
        public int DocumentCountNew { get; set; }

        public PdfDocumentRepository()
        {
            _lastInstalledVersion =
                GhostscriptVersionInfo.GetLastInstalledVersion(
                    GhostscriptLicense.AFPL | GhostscriptLicense.GPL,
                    GhostscriptLicense.GPL);
            var mc = new MongoClient(ConfigurationManager.AppSettings["mongourl"]);

            _mcMainDb = mc.GetDatabase("vogsphere");
            _mcFileDb = mc.GetDatabase("vogsphere_files");
        }

        public PdfDocument GetDocument(Guid id)
        {
            return _mcMainDb.GetCollection<PdfDocument>("pdfs").Find(x => x.DocumentId == id).FirstOrDefault();
        }

        public byte[] GetThumbnail(Guid docid)
        {
            var doc = _mcMainDb.GetCollection<PdfDocument>("pdfs").Find(x => x.DocumentId == docid).FirstOrDefault();
            if (doc == null) throw new ArgumentException("Document not found");

            var bucket = new GridFSBucket(_mcFileDb);
            return bucket.DownloadAsBytes(doc.ThumbObjectId);
        }

        public byte[] GetPdf(Guid docid)
        {
            var doc = _mcMainDb.GetCollection<PdfDocument>("pdfs").Find(x => x.DocumentId == docid).FirstOrDefault();
            if (doc == null) throw new ArgumentException("Document not found");

            var bucket = new GridFSBucket(_mcFileDb);
            return bucket.DownloadAsBytes(doc.PdfObjectId);
        }


        public List<PdfDocument> ListNew()
        {
            return _mcMainDb.GetCollection<PdfDocument>("pdfs").Find(x => x.IsNew).ToList();
        }

        public void CreateFromFile(string pdffilename)
        {
            var bucket = new GridFSBucket(_mcFileDb);
            var fi = new FileInfo(pdffilename);


            var taskUploadPdf = bucket.UploadFromBytesAsync(fi.Name, File.ReadAllBytes(pdffilename));

            var thumbFileName = fi.Name + "-thumb.png";
            string physThumbfile = Path.Combine(TempPath, thumbFileName);
            
            //Create the thumbnnail
            int desired_x_dpi = 300;
            int desired_y_dpi = 300;
            using (GhostscriptRasterizer rasterizer = new GhostscriptRasterizer())
            {
                rasterizer.Open(pdffilename, _lastInstalledVersion, true);
                using (Image img = rasterizer.GetPage(desired_x_dpi, desired_y_dpi, 1))
                {
                    img.Save(physThumbfile, ImageFormat.Png);
                }
            }
            var taskUploadThumb = bucket.UploadFromBytesAsync(thumbFileName, File.ReadAllBytes(physThumbfile));

            //do ocr
            //Bitmap image = new Bitmap(physThumbfile);
            //List<string> words = new List<string>();

            //Tesseract ocr = new Tesseract();
            //try
            //{
            //    ocr.Init(OcrDataPath, "deu", false); // To use correct tessdata
            //    List<Word> result = ocr.DoOCR(image, Rectangle.Empty);
            //    words.AddRange(from word in result where char.IsUpper(word.Text[0]) && word.Text.Length > 3 select word.Text);
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e);
            //}

            string hash = ComputeHash(pdffilename);
            PdfDocument doc = new PdfDocument
            {
                DocumentId = Guid.NewGuid(),
                DateScanned = fi.CreationTimeUtc,
                FileName = fi.Name,
                Keywords = new List<string>(),
                FileHash = hash,
                PdfObjectId = taskUploadPdf.Result,
                ThumbObjectId = taskUploadThumb.Result,
                DateImported = DateTime.UtcNow,
                IsNew = true
            };

            _mcMainDb.GetCollection<PdfDocument>("pdfs").InsertOne(doc);
        }

        private string ComputeHash(string file)
        {
            using (FileStream fs = new FileStream(file, FileMode.Open))
            using (BufferedStream bs = new BufferedStream(fs))
            {
                using (SHA1Managed sha1 = new SHA1Managed())
                {
                    byte[] hash = sha1.ComputeHash(bs);
                    StringBuilder formatted = new StringBuilder(2 * hash.Length);
                    foreach (byte b in hash)
                    {
                        formatted.AppendFormat("{0:X2}", b);
                    }
                    return formatted.ToString();
                }
            }
        }

        public void SaveDocument(PdfDocument doc)
        {
            var mc = _mcMainDb.GetCollection<PdfDocument>("pdfs");

            var dbDoc = mc.Find(x => x.DocumentId == doc.DocumentId).FirstOrDefault();
            dbDoc.IsNew = false;
            dbDoc.Keywords = doc.Keywords;
            dbDoc.Sender = doc.Sender;
            dbDoc.Subject = doc.Subject;
            dbDoc.DocumentDate = doc.DocumentDate;
            mc.ReplaceOne(x => x.DocumentId == doc.DocumentId, dbDoc);
        }

        public Guid NextNewDocumentId()
        {
            var newdoc = _mcMainDb.GetCollection<PdfDocument>("pdfs").Find(x => x.IsNew).FirstOrDefault();
            return newdoc?.DocumentId ?? Guid.Empty;
        }

        public List<PdfDocument> Search(string mode, string query)
        {
            var mc = _mcMainDb.GetCollection<PdfDocument>("pdfs");
            Expression<Func<PdfDocument, bool>> filter = null;
            switch (mode)
            {
                case "tag":
                    filter = x => x.Keywords.Contains(query);
                    break;
                case "subject":
                    filter = x => x.Subject.Contains(query);
                    break;
                case "sender":
                    filter = x => x.Sender.Name.Contains(query);
                    break;
                default:
                    filter = x => !x.IsNew;
                    break;

            }
            
            return mc.Find(filter).ToList();
        }

        public IEnumerable<string> DistinctKeywords()
        {
            BsonDocument bdoc = _mcMainDb.GetCollection<PdfDocument>("pdfs")
                .Aggregate().Unwind(x => x.Keywords)
                .Group(new BsonDocument {{"_id", "1"}, {"keywords", new BsonDocument("$addToSet", "$Keywords")}})
                .FirstOrDefault();

            return bdoc?["keywords"].AsBsonArray.Select(x => x.AsString) ?? new List<string>();
        }

        public void DeleteDocument(Guid docid)
        {
            var mc = _mcMainDb.GetCollection<PdfDocument>("pdfs");
            mc.DeleteOne(x => x.DocumentId == docid);
        }
    }
}