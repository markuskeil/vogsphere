using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;


namespace Vogsphere.Models
{
    [BsonIgnoreExtraElements]
    public class PdfDocument
    {
        public Guid DocumentId { get; set; }
        public ObjectId PdfObjectId { get; set; }
        public string FileName { get; set; }
        public DateTime DateScanned { get; set; }
        public string FileHash { get; set; }
        public ObjectId ThumbObjectId { get; set; }
        public DateTime DateImported { get; set; }
        public bool IsNew { get; set; }


        public List<string> Keywords { get; set; }
        public double InvoiceAmount { get; set; }
        public DocumentType Type { get; set; }
        public AddressInfo Sender { get; set; }
        public string Subject { get; set; }
        public DateTime DocumentDate { get; set; }

        public PdfDocument()
        {
            Keywords = new List<string>();
            Type = DocumentType.Other;
        }

    }


    public class AddressInfo
    {
        public string Name { get; set; }
        public string City { get; set; }
        public string CustomerNumber { get; set; }
    }

    public enum DocumentType
    {
        Invoice = 1, Contract = 2, Other = 0
    }
}